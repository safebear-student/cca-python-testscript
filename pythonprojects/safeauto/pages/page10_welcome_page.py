# Import PageObject and PageElement to use below
from page_objects import PageObject, PageElement

# Create a WelcomePage class that inherits all the functionality from the PageObject framework
class WelcomePage(PageObject):

    # Create a Login Link element by telling selenium to search for the Link text "Login"
    login_link = PageElement(link_text="Login")

    # Create a method that checks that we're on the correct page by confirming that the page title is 'Welcome'
    def check_page(self):
        # This returns 'True' if 'Welcome' is in the page title, and 'False' if it isn't.
        return "Welcome" in self.w.title

    # Create a method that clicks on the login link. Pass through the loginpage object from the test case so we can use
    # the check_page() method within it to confirm that we're taken to the login page after clicking on the link.
    def click_login(self, lpage):
        # Click on the login link Page Element. We told selenium how to find this at the top of the class.
        self.login_link.click()
        # Use the check_page() method in the LoginPage class to confirm we're not on the login page.
        return lpage.check_page()
