# Import the usual from page_objects
from page_objects import PageObject, PageElement

# Not every PageObject needs to be a Web Page! This one is a Frame.
class MainFramePage(PageObject):

    # Find the safebear logo by tag name as it's the only image in the frame
    safebear_logo_image = PageElement(tag_name = 'img')

    # The usual method to check that we're in the correct frame. This time we check that the SafeBear logo is displayed.
    def check_page(self):
        return self.safebear_logo_image.is_displayed()

    # This method clicks on the SafeBear logo.
    def clickOnLogo(self):
        self.safebear_logo_image.click()

