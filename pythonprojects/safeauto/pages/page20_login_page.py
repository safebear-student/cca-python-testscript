# Import PageObject, PageElement and also MultiPageElement
from page_objects import PageObject, PageElement, MultiPageElement

# Create a class for the login page inheriting all the features from the PageObject class
class LoginPage(PageObject):

    # This is where we create all our Page Elements. I.e. we're telling selenium how to locate them in the DOM.

    # Home link at the top of the page, locate by link_text
    home_link = PageElement(link_text="Home")

    # Username field, locate by id
    username_field = PageElement(id_="myid")
    # Password field, locate by id also.
    password_field = PageElement(id_="mypass")

    # Find multiple elements by type using xpath. The XPath finds all Elements with the <input> tag and then narrows it
    # down further by finding only those with attribute 'type' = 'text' i.e. <input type="text">
    # This way, we know it's definitely a text field when we need to typy in it.
    # The only two text fields on the screen are the username and password, so this will find these.
    username_and_password_fields = MultiPageElement(xpath = '//input[@type="text"]')

    # The usual method to confirm we're on the correct page
    def check_page(self):
        return "Sign" in self.w.title

    # A method to click on the Home link in the ribbon menu at the top of the login page. This needs to have the Welcome
    # Page object passed through to it because after you click on 'Home' you should be taken to the Welcome Page. We
    # need to confirm this.
    def click_home(self,wpage):
        self.home_link.click()
        return wpage.check_page()

    # This method types the username and password passed through from the test case into the username and password
    # fields. It then checks that you're now on the login page.
    def login(self, mpage, username, password):
        # Type the username into the username field.
        self.username_field.send_keys(username)
        # Then enter the password.
        self.password_field.send_keys(password)
        # Then submit. Notice that you don't need to find the submit button. Selenium knows that the Username and
        # Password fields are in a form and will submit them for you.
        self.password_field.submit()
        # Confirm we're now logged in and on the main page.
        return mpage.check_page()

    # This method will populate all text input fields with the text passed through from the test case and then check
    # that this text is present in the fields.
    def multi_page_elements_test(self, text):
        # Send the text to the username and password fields. Note that we didn't use send_keys this time. We could have,
        # but it is also possible just to say PageElement = 'some text' and it will work the same as send_keys if the
        # PageElement is a Text Field. I prefer send_keys() as this is easier to read.
        self.username_and_password_fields = text
        # Confirm that the username and password have the correct text in them
        return text in self.username_field.get_attribute("value") and text in self.password_field.get_attribute("value")
