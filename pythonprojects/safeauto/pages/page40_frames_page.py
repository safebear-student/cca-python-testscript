# Import PageObject and PageElement again
from page_objects import PageObject, PageElement
# Import the MainFramePage class so that we can use the methods in it
from page41_frames_page_main_frame import MainFramePage

# Set up a FramesPage PageObject
class FramesPage(PageObject):

    # Identify the main frame on the page by its name.
    main_frame = PageElement(name = 'main')

    # The usual method for checking that we're on the correct page
    def check_page(self):
        return "Frame" in self.w.title

    # We need to switch into a frame before we can perform any actions on the elements within it.
    def go_to_main_frame(self):
        # We use the driver method switch_to_frame() to do this.
        self.w.switch_to_frame(self.main_frame)
        # Create a MainFramePage object so we can use the methods within it and pass through the driver.
        mainframe = MainFramePage(self.w)
        # Use the clickOnLogo method within the MainFramePage object.
        mainframe.clickOnLogo()
        # Confirm that we're in the correct frame.
        return mainframe.check_page()

    # A method to return us to the previous window using the window_handle class variable that gets passed to us from
    # the test case.
    def move_back_to_previous_window(self,mpage,handle):

        # The WebDriver uses the handle we passed through to switch back to the previous page.
        self.w.switch_to_window((handle))
        # Now let's check we're back on the main page.
        return mpage.check_page()
