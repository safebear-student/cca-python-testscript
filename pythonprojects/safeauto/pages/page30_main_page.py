# The 'time' package is useful to enter some 'sleeps' into a script. But be careful they can massively slow down your
# test
import time
# Again we import the usual from page_objects
from page_objects import PageObject, PageElement
# The 'Select' class is used to select values from dropdowns
from selenium.webdriver.support.ui import Select
# 'WebDriverWait' and 'expected_conditions' are used to explicitly wait a certain amount of time for something to appear
# on screen. You use this if something takes longer to load than everything else.
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


# Again our page inherits the PageObject class.
class MainPage(PageObject):

    # Sets a class variable called 'window_handle' that we'll use later to store the current window handle before we
    # switch to the Frames Page
    window_handle = 0

    # Find the Logout link a the top of the page by its link text
    logout_link = PageElement(link_text="Logout")

    # Find the Say Something button by its css
    saysomething_button = PageElement(css = 'button.btn.btn-lg.btn-success')

    # Find the Say Something field by its id
    saysomething_field = PageElement(id_ = 'saySomething')

    # Find the two dropdowns by their names in the DOM
    first_add_dropdown = PageElement(name = 'num1')
    second_add_dropdown = PageElement(name = 'num2')

    # Find the Result field by its id
    result_field = PageElement(id_ = 'addResult')

    # Find the Submit button by its css
    dropdowns_submit_button = PageElement(css = 'input.btn.btn-lg.btn-success')

    #find the 'Go to frames' button by partial link_text
    go_to_frames_button = PageElement(partial_link_text = 'frames')

    # The usual method to check that we're on the correct page
    def check_page(self):
        return "Logged" in self.w.title

    # A method to click on the Logout link at the top of the page
    def logout(self,wpage):
        self.logout_link.click()
        # Confirm we're logged out by checking that we're on the Welcome Page
        return wpage.check_page()

    # A method to click on the Say Something button, send some text to the pop-up (alert) box that appears and then
    # confirm that the text is returned into the Say Something field.
    def press_saysomething_button_enter_text_into_alert(self, text):
        # Click on the Say Something button
        self.saysomething_button.click()
        # This is where we'll use our explicit wait. You can see that we need to pass WebDriverWait the driver (w) that
        # we're using and also how long (in seconds) that you want the driver to wait for.
        # If the driver times out (i.e. waits for longer than 10 seconds for the element to appear) then it throws an
        # exception. We need to tell Python what to do when it receives the exception.
        try:
            # Wait for 10 seconds until the expected condition (EC) is satisfied. I.e. that an alert (pop-up) box is
            # present.
            element = WebDriverWait(self.w, 10).until(EC.alert_is_present())
        except TimeoutError:
            # If it doesn't appear in 10 seconds, print this text to screen.
            print("Waited 10 seconds and pop-up didn't appear")
        # Assuming the alert did appear, switch to the alert
        alert = self.w.switch_to_alert()
        # Send the alert some text. This has been passed through from the Test Case.
        alert.send_keys(text)
        # This is an example of a sleep (for 1 second).
        time.sleep(1)
        # Press the OK button on the pop-up box by using the following code.
        alert.accept()
        # Sleep again for 1 second. These are just her as examples. Sometimes sleeps can be useful when debugging as it
        # allows  you time to see what's happening on screen.
        time.sleep(1)
        # Confirm that the text is returned in the Say Something field.
        return text in self.saysomething_field.text

    # A method to open the Frames page and then switch to it. You change pages using window handles.
    def open_and_move_to_frames_page(self, fpage):
        # Store the window handle of the current page in the class variable so that we can switch back to it afterwards.
        self.window_handle = self.w.current_window_handle
        # Click on the 'Go to frames' button
        self.go_to_frames_button.click()
        # This is the standard 'for' loop that will take you to the new browser window that has opened. In this case it
        # will be the Frames Page.
        for handle in self.w.window_handles:
            self.w.switch_to_window(handle)
        return fpage.check_page()

    def select_number_from_dropdown(self, value1, value2, result):
        # This selects a number from a dropdown
        # You must provide it with two values to add together (value1,value2,result).
        # E.g. 2,2,4 (where 2 + 2 = 4)
        # This function uses these numbers to select the dropdown values and then submits them.
        # It then checks the result is correct.

        # First of all we Select the first dropdown
        select1 = Select(self.first_add_dropdown)
        # Then the second
        select2 = Select(self.second_add_dropdown)

        # Then we use the visible text to select the value of the first dropdown. We want the number 2, but the numbers
        # in the dropdown are stored as text not as numbers. So we must use the 'str()' method to convert our integers
        # to strings (numbers to text).
        select1.select_by_visible_text(str(value1))
        # This time we use the 'value' attribute of the number in dropdown to select it. Again this is stored as a
        # string in the DOM.
        select2.select_by_value(str(value2))
        # Click on the Submit button.
        self.dropdowns_submit_button.click()
        # Again we have a sleep here which gives you a second to see what's happening. It's not needed.
        time.sleep(1)
        # Confirm that the result is '4'. Again we have to convert to a string as the results field stores everything
        # as a string.
        return str(result) in self.result_field.text







