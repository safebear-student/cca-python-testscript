import base_test

class TestCases(base_test.BaseTest):

    def test_01_test_login_page_login(self):
        """Documentation: This test checks that you can navigate to the login page and login
        Given that I've clicked on the Login link on the Welcome Page
        When I log in with valid credentials
        Then I am taken to the main (logged in) page.
        """
        # Step 1: Click on the Login link on the Welcome Page. Check you're taken to the Login Page.
        assert self.welcomepage.click_login(self.loginpage)
        # Step 2: Enter 'testuser' as the username and 'testing' as the password, then login. Confirm that you're
        # taken to the Main (Logged in) Page.
        assert self.loginpage.login(self.mainpage, "testuser", "testing")
        # Step 3: Log out of the page so the next test can start. Confirm that you're taken to the welcome page.
        assert self.mainpage.logout(self.welcomepage)

    def test_02_test_login_page_multielements(self):
        """Documentation: This test checks that you can find multiple elements on a page and send text to them
        Given that I've clicked on the Login link on the Welcome Page
        When I search for 'input' fields that are type text and type 'test' in them
        Then all the input fields are populate with the words 'test'.
        """
        # Step 1: Click on the Login link on the Welcome Page. Check you're taken to the Login Page.
        assert self.welcomepage.click_login(self.loginpage)
        # Step 2: Find all the input fields that are type 'text', send them a string 'test' and confirm that they're
        # populated with this string.
        assert self.loginpage.multi_page_elements_test("test")
        # Step 3: Click on Home
        # We can't log out as we haven't logged in, so click on the 'Home' link to return to the Welcome Page ready
        # for the next test.
        assert self.loginpage.click_home(self.welcomepage)

    def test_03_test_main_page_javascript_input(self):
        """Documentation: This test checks that you can find a JavaScript pop-up (alert) box and send text to it.
        Given that I'm logged in
        When I click on the 'Say Something' button
        Then I can type in some text and that text is sent back to me on screen
        """
        # Step 1: Click on the Login link on the Welcome Page. Check you're taken to the Login Page.
        assert self.welcomepage.click_login(self.loginpage)
        # Step 2: Log in to the webpage
        assert self.loginpage.login(self.mainpage, "testuser", "testing")
        # Step 3: Click on the 'Say Something' button and enter "Hello" into the pop-up (JavaScript alert) that appears.
        assert self.mainpage.press_saysomething_button_enter_text_into_alert("Hello")
        # Step 4: Log out of the page so the next test can start. Confirm that you're taken to the welcome page.
        assert self.mainpage.logout(self.welcomepage)

    def test_04_test_main_page_frames(self):
        """Documentation: This test checks that you can switch to the Main Frame on the Frames page.
        Given that I'm logged in
        And that I've clicked on the 'Go to frames' button
        When I switch to the Main Frame and click on the SafeBear logo
        Then the SafeBear website appears.
        """
        # Step 1: Click on the Login link on the Welcome Page. Check you're taken to the Login Page.
        assert self.welcomepage.click_login(self.loginpage)
        # Step 2: Login
        assert self.loginpage.login(self.mainpage, "testuser", "testing")
        # Step 3: Click on the 'Go to frames' button, change windows to the Frames Page and confirm you're on the right
        # page
        assert self.mainpage.open_and_move_to_frames_page(self.framepage)
        # Step 4: Switch to the Main Frame and click on the SafeBear logo
        assert self.framepage.go_to_main_frame()
        # Step 5: Move back to the the Main (Logged in) Page. Notice that we're passing through the main page object but
        # also the class variable from the main page called 'window handle' so that the method knows what window to move
        # back to.
        assert self.framepage.move_back_to_previous_window(self.mainpage, self.mainpage.window_handle)
        # Step 6: Log out of the page so the next test can start. Confirm that you're taken to the welcome page.
        assert self.mainpage.logout(self.welcomepage)

    def test_05_test_main_page_dropdowns(self):
        """Documentation: This test checks that you can use the dropdown boxes on the Main Page.
        Given that I'm logged in
        When I choose the number '2' in each of the dropdown boxes and then clicked submit
        Then the result I get back is '4'.
        """
        # Step 1: Click on the Login link on the Welcome Page. Check you're taken to the Login Page.
        assert self.welcomepage.click_login(self.loginpage)
        # Step 2: Login
        assert self.loginpage.login(self.mainpage, "testuser", "testing")
        # Step 3: select '2' and '2' in both dropdowns and then confirm the result is '4'
        assert self.mainpage.select_number_from_dropdown(2,2,4)
        # Step 4: Logout.
        assert self.mainpage.logout(self.welcomepage)
