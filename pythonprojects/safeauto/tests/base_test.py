# Import the unittest framework which will recognise the setUp, tearDown and test methods
import unittest

# Import the parameters class and all the Page Classes we've created
from utils import Parameters
from page10_welcome_page import WelcomePage
from page20_login_page import LoginPage
from page30_main_page import MainPage
from page40_frames_page import FramesPage


# Create a BaseTest class and also inherit all the features from the TestCase class in unittest.
class BaseTest(unittest.TestCase):

    # Initialize the parameters and Page Objects.
    param = Parameters()
    # The page objects need to be passed the driver (w) and the URL of the website we're going to use (rootUrl)
    welcomepage = WelcomePage(param.w, param.rootUrl)
    loginpage = LoginPage(param.w, param.rootUrl)
    mainpage = MainPage(param.w, param.rootUrl)
    framepage = FramesPage(param.w, param.rootUrl)


    # Before every test, the driver will open the browser (if not already open) and navigate to the URL
    # Then it'll maximise the window (if not already maximised)
    # And then check that we're on the correct page (Welcome Page)
    # Before running the test
    def setUp(self):
        self.param.w.get(self.param.rootUrl)
        self.param.w.maximize_window()
        assert self.welcomepage.check_page()

    # After all the tests have run in the calls, the following method is called that quits the browser.
    @classmethod
    def tearDownClass(cls):
        cls.param.w.quit()

# This is standard boilerplate code for the main class in Python. It's not really needed here, but is good practice to
# include it anyway.
if __name__ == "__main__":
    unittest.main()
