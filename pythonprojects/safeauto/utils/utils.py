# Import the WebDriver from selenium to drive the brower
from selenium import webdriver
# Import Desired Capabilities to run the Selenium Server. We're using it here to run headless with HtmlUnit, however
# You can also use it to run tests simultaneously on different browsers
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# Set up our Parameter's class
class Parameters():
    def __init__(self):
        # Set up the WebDriver to run Chrome.
        self.w = webdriver.Chrome()
        # Set up the URL of our Website
        self.rootUrl = "http://automate.safebear.co.uk"

        # Selenium Server
        # This is the bit of code that tells selenium to drive Selenium Server on Port 4444 with HtmlUnit (headless).
        # You need to uncomment the code and then comment out the self.w = webdriver.Chrome() line above in order
        # to use it.
        # self.w = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', desired_capabilities=DesiredCapabilities.HTMLUNITWITHJS)

        # implicit waits
        # An implicit wait tells WebDriver to poll the DOM for a certain amount of time when trying to find any element
        # (or elements) not immediately available. The default setting is 0. Once set, the implicit wait is set for the
        # life of the WebDriver object.
        self.w.implicitly_wait(5)
